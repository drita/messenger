package management;

import java.util.ArrayList;

import model.Nachricht;
import model.User;

/**
 * @author Drita Mahmudi
 */
public class NachrichtManagement {
	/**
	 * speichert eine liste von Nachrichten fur jede user
	 */
	private ArrayList<Nachricht> arrayNachricht = new ArrayList<Nachricht>();
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * nachrichten hinzufugen 
	 * @param text
	 * @param zuUser
	 * @param vomUser
	 * @param id
	 */
	public void nachrichtHinzufugen(String text, User zuUser, User vomUser){
		 int id = arrayNachricht.size()+ 1;
		 
		Nachricht nachricht = new Nachricht(text, zuUser, vomUser, id);
		if(!text.trim().isEmpty()){
			arrayNachricht.add(nachricht);
		}
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * findet nachricht zwischen user
	 * @param vomUser
	 * @param zuUser
	 * @return
	 */
	public ArrayList<Nachricht> findeNachrichtenZwischenUser(User vomUser, User zuUser){
		
		ArrayList<Nachricht> findeNachrichtZwischenUser = new ArrayList<Nachricht>();
		
		for(int i = 0; i < arrayNachricht.size(); i++){
			if(arrayNachricht.get(i).getVomUser().getUsername().equals(vomUser.getUsername()) && arrayNachricht.get(i).getZuUser().getUsername().equals(zuUser.getUsername())){
				findeNachrichtZwischenUser.add(arrayNachricht.get(i));
			}
		}
		return findeNachrichtZwischenUser;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
}
