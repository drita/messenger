package management;

import java.util.ArrayList;

import model.User;

/**
 * Verwaltet User
 * 
 * @author Drita Mahmudi a0900394
 * 
 */
public class UserManagement {
	/**
	 * speichert liste von User
	 */
	private ArrayList<User> arrayUser = new ArrayList<User>();

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * pruft ob der username schon existiert
	 * 
	 * @param username
	 * @return false wenn user nicht existiert
	 */
	public boolean userExist(String username) {

		for (int i = 0; i < arrayUser.size(); i++) {

			if (arrayUser.get(i).getUsername().equals(username)) {
				return true;
			}
		}
		return false;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * die methode pruft ob der benutzer existier und sein password stimmt
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean login(String username, String password) {
		if(!userExist(username)){
			return false;
		}
		
		for(int i = 0; i < arrayUser.size(); i++){
			if(arrayUser.get(i).getPassword().equals(password) && arrayUser.get(i).getUsername().equals(username)){
				return true;
			}
		}
		return false;
	}
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * eine neue user einlegen
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean registereUser(String username, String password) {
		User user = new User(username, password);
		
		if(username.trim().isEmpty() || password.trim().isEmpty() || userExist(username))
			return false;
		arrayUser.add(user);
		
		return true;
	}
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	/**
	 * die funktion geht die arrayUser durch und holt alle 
	 * UserNames
	 * @return eineListe mit allen UserName
	 */
	public ArrayList<String> getUserList(){
		
		ArrayList<String> userNames = new ArrayList<String>();
		
		for(int i = 0; i < arrayUser.size(); i++){
			
			 userNames.add(arrayUser.get(i).getUsername());
		}
		return userNames;
	}
}