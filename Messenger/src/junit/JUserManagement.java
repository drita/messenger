package junit;

import static org.junit.Assert.*;
import management.UserManagement;

import org.junit.Test;



public class JUserManagement{

	private UserManagement userManagement = new UserManagement();
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * pruft ob der username schon existiert
	 * @param username
	 * @return
	 */
	@Test
	public void testUserExist(){
		
	     
		//pruffe ob user in liste
		boolean test = userManagement.userExist("drita");
		
		userManagement.registereUser("miki", "1234");
		
		boolean test1 = userManagement.userExist("miki");
		
		
		assertEquals(false, test);
		assertEquals(true, test1);
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * die methode pruft ob der benutzer existier und sein password stimmt
	 * @param username 
	 * @param password
	 * @return
	 */
	@Test
	public void testLogIn(){
		
		boolean test = userManagement.registereUser("miki", "1234");
		userManagement.registereUser("alfred", "ABCD");
		
		assertEquals(true, test);
		
		boolean test1 = userManagement.login("miki", "1234");
		boolean test2 = userManagement.login("miki", "12345f");
		boolean test3 = userManagement.login("pipi", "1548");
		boolean test4 = userManagement.login("alfred", "1234");
		
		assertEquals(true, test1);
		assertEquals(false, test2);
		
		assertEquals(false, test3);
		assertEquals(false, test4);
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * eine neue user einlegen
	 * @param username
	 * @param password
	 * @return
	 */
	@Test
	public void testRegistereUser(){
		
		boolean test = userManagement.registereUser("hans", "4589");
		
		boolean test1 = userManagement.registereUser(" ", "789");
		boolean test2 = userManagement.registereUser("hans", " ");
		
		boolean test3 = userManagement.registereUser("hans", "rfrgr");
		
		assertEquals(true, test);
		
		assertEquals(false, test1);
		assertEquals(false, test2);
		
		assertEquals(false, test3);
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	

}
