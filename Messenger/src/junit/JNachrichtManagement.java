package junit;

import java.util.ArrayList;

import static org.junit.Assert.*;
import management.NachrichtManagement;
import model.Nachricht;
import model.User;

import org.junit.Test;


public class JNachrichtManagement {
	
	private NachrichtManagement nachrichtManagement = new NachrichtManagement();
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * speichert den nachricht, denen die user
	 *  ein anderen geschickt haber, sowie deren username
	 */
	@Test
	public void testNachrichtHinzufugen(){
		String nachricht1 = "bist du da" + "das bin ich";
		String nachricht2 = "bis du du";
		String ausgabe = "";
		
		User user1 = new User("drita");
		User user2 = new User("hans");
		User user3 = new User("pipi");
		
		nachrichtManagement.nachrichtHinzufugen(nachricht1, user1, user2);
		nachrichtManagement.nachrichtHinzufugen(nachricht2, user1, user2);
		
		
		ArrayList<Nachricht> test2 = nachrichtManagement.findeNachrichtenZwischenUser(user1, user2);
		
		for(int i = 0; i < test2.size(); i++){
			ausgabe += test2.get(i).getText();
		}	
			assertEquals(nachricht1+nachricht2, ausgabe);
	
	}

}
