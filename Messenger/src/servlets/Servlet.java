package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import management.NachrichtManagement;
import management.UserManagement;



@WebServlet("/Servlet")
public class Servlet extends HttpServlet{
	
	public static UserManagement userManagement = new UserManagement();
	public static NachrichtManagement nachrichtManagement = new NachrichtManagement();
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String aufgabe = request.getParameter("aufgabe");
	
		//wenn User eingelogt
		if(session.getAttribute("username") != null){
				if(request.getParameter("logout") != null){
					session.invalidate();
					request.getRequestDispatcher("/WEB-INF/StartSeite.jsp").include(
							request, response);
				}
				else{
					if(aufgabe==null){
						request.getRequestDispatcher("/WEB-INF/HauptSeite.jsp").include(request, response);
						}
					else if(aufgabe.equals("Chat")){
						request.getRequestDispatcher("/WEB-INF/Chat.jsp").include(
								request, response);
								return;						
						}
						
					else{
							request.getRequestDispatcher("/WEB-INF/HauptSeite.jsp").include(request, response);
						}	
			}
		}
	}
}