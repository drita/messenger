package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Nachricht;
import model.User;

@WebServlet("/NachrichtHinzufugenServlet")
public class NachrichtHinzufugenServlet extends HttpServlet {
		
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);

		String text = request.getParameter("text");
		
		// wie bekomme ich den eingelogte nutzer als User object???
		User vomUser = new User ();
		//hier bekomme den eingelogte user
		vomUser.setUsername((String)session.getAttribute("username"));
		User zuUser = new User();
		
		zuUser.setUsername(request.getParameter("zuUsername"));
	
		Servlet.nachrichtManagement.nachrichtHinzufugen(text, zuUser, vomUser);
		
		
		//hier will die nachrichten zuruck geben beim chatfenster
		ArrayList<Nachricht> nachrichten = Servlet.nachrichtManagement.findeNachrichtenZwischenUser(vomUser, zuUser);
		session.setAttribute("text", nachrichten);
		
		request.getRequestDispatcher("/WEB-INF/Chat.jsp").include(
				request, response);

	}
}
