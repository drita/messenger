	package servlets;

	import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

	@WebServlet("/LogginServlet")
	public class LogginServlet extends HttpServlet {
		/**
		 * die methode logt in den benutzer, der sich schon registriert hat
		 * @param request anfrage
		 * @param response antwort
		 * @throws ServletException exeption
		 * @throws IOException ioexception
		 */
		public void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {	
			HttpSession session = request.getSession(true);
			
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			
			if(Servlet.userManagement.login(username, password)){
				session.setAttribute("username", username);
				session.setAttribute("userManagement", Servlet.userManagement);
				session.setAttribute("nachrichtManagement", Servlet.nachrichtManagement);
				
				request.getRequestDispatcher("/WEB-INF/HauptSeite.jsp")
				.include(request, response);
			}
			else
				request.getRequestDispatcher("/WEB-INF/StartSeite.jsp").include(
					request, response);	
		}//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}
