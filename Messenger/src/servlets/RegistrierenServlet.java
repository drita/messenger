	package servlets;

	import java.io.IOException;
	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;


	@WebServlet("/Register")
	public class RegistrierenServlet extends HttpServlet {
		/**
		 * registriert neue benutzer
		 * @param request anfrage
		 * @param response antwort
		 * @throws ServletException exeption
		 * @throws IOException exeption
		 */
	
		public void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
		
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			
			Servlet.userManagement.registereUser(username, password);
			
			request.getRequestDispatcher("/WEB-INF/StartSeite.jsp").include(
					request, response);
		}
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}
