package model;
/**
 * speichert eine User
 * 
 * @author Drita Mahmudi
 */
/**
 * @author Dr
 *
 */
public class User {
	
	private String username;
	private String password;
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * default constructor
	 */
	public User(){
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * parameter constructor
	 * @param username
	 * @param password
	 */
	public User(String username, String password){
		setUsername(username);
		setPassword(password);
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * parameter constructor
	 * @param username
	 */
	public User(String username){
		setUsername(username);
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den User Password zuruck
	 * @return
	 */
	public String getPassword() {
		return password;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 *Pr�fen Sie beim Eintragen des Passworts (Methode�setPassword), 
	 *ob die L�nge der Eingabe > 6 ist und kein Leerzeichen enth�lt.	
	 * setzt den User password
	 * @param password
	 */
			public void setPassword(String password) {
			if (password.length() < 6 || password.contains(" ")){
				throw new IllegalArgumentException("Passwort kleiner als 6 oder leerzeichnen. Versuchen sie es noch mal :) ");
			}
				
			else 
			 this.password = password;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt User name
	 * @return
	 */
	public String getUsername() {
		return username;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * @param password to verify password
	 * @return true or false
	 */ 
	boolean verifyPassword(String password){
		
		if(password.equals(this.password)){
			return true;
		}
		else
			return false;
		
		
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt user Name
	 * @param name
	 */
	public void setUsername(String name) {
				if(name == null || name.isEmpty() || name.length() < 6)
					throw new IllegalArgumentException("Username darf nicht null, Empty or kleiner als 6 Zeichnen sein! ");

				String newName = "";
				
				for(int i = 0; i < name.length(); i++)
				{
					char original = name.charAt(i);
					char c = Character.toLowerCase(original);
					
					if(c < 'a' || c > 'z')
						throw new IllegalArgumentException("Username darf nur zeichnen A-Z enthalten!");
					
					newName += original;
				}
				
				this.username = newName;
	}			
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

}