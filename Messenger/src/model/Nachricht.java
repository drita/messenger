package model;

/**
 * speichert eine nachricht und den User vomUser und zuUser
 * 
 * @author Drita Mahmudi
 */
/**
 * @author Dr
 *
 */
public class Nachricht {
	private String text;
	private User vomUser;
	private User zuUser;
	private int id;

	/**
	 * default konstruktor
	 */
	public Nachricht(){
	}
	
	/**
	 * Parameter Konstruktor
	 * @param text
	 * @param vomUser
	 * @param zuUser
	 * @param id
	 */
	public Nachricht(String text, User vomUser, User zuUser, int id){
		setText(text);
		setVomUser(vomUser);
		setZuUser(zuUser);
		setId(id);
	}
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den text dieser nachricht zuruck
	 * @return text
	 */
	public String getText() {
		return text;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den TEXT dieser Nachricht
	 * @param text
	 */
	public void setText(String text) {
		if(text == null || text.isEmpty())
			throw new IllegalArgumentException("Text darf nicht null or Empty sein! ");

		this.text = text;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * get User vom wem die Nachricht kommt
	 * @return
	 */
	public User getVomUser() {
		return vomUser;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt User vom wem die Nachricht kommt
	 * @param vomUser
	 */
	public void setVomUser(User vomUser) {
		this.vomUser = vomUser;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * get den User der den Nachricht bekommen hat
	 * @return
	 */
	public User getZuUser() {
		return zuUser;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den User fur wem die Nachricht ist
	 * @param zuUser
	 */
	public void setZuUser(User zuUser) {
		this.zuUser = zuUser;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
}
